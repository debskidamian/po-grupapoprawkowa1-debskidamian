package pl.edu.uwm.wmii.debskidamian.laboratorium05;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie4 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        ArrayList<Integer> ListaTablicowa = new ArrayList<Integer>();
        System.out.println("Podaj dlugosc listy tablicowej: ");
        int dlugoscListyTablicowej = input.nextInt();

        for (int i = 0; i < dlugoscListyTablicowej; i++) {
            System.out.print("Podaj elementy pierwszej listy: ");
            ListaTablicowa.add(input.nextInt());
        }

        input.close();

        System.out.println("Zawartość pierwszej listy tablicowej: " + ListaTablicowa);

        ArrayList<Integer> OdwroconaListaTab = reversed(ListaTablicowa);
        System.out.print("Lista tablicowa po odwróceniu: " + OdwroconaListaTab);

    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {

        for(int i = 0, j = a.size() - 1; i < j; i++) {
            a.add(i, a.remove(j));
        }
        return a;
    }
}
