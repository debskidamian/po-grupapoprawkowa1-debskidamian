package pl.edu.uwm.wmii.debskidamian.laboratorium01;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Samochod {

    String marka;
    int rocznik;
    float pojSilnika;

    void obliczSpalanieNaTrasie(int trasa){
        System.out.println("Wynik spalania = " + trasa * (5 * pojSilnika));
    }

    public Samochod(String marka, int rocznik, float pojSilnika){
        this.marka = marka;
        this.rocznik = rocznik;
        this.pojSilnika = pojSilnika;
    }

    void ustawRocznik(int rocznik){
        while (rocznik < 1900 || rocznik > 2018){
            System.out.println("Podaj rocznik z przedziału 1900-2018");
        }
    }


}
