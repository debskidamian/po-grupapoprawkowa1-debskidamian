package pl.edu.uwm.wmii.debskidamian.laboratorium01;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {


        List<Samochod> lista = new ArrayList<Samochod>();
        lista.add(new Samochod("Mercedes", 2002, 4));
        lista.add(new Samochod("BMW", 2015, 3));
        lista.add(new Samochod("Fiat", 2012, 2));

        for (Samochod auta : lista) {
            System.out.println(auta.marka + " " + auta.rocznik + " " + auta.pojSilnika);
            auta.obliczSpalanieNaTrasie(30);
        }

    }
}
