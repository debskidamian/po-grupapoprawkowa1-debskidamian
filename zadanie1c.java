package pl.edu.uwm.wmii.debskidamianlab04;

import java.util.Scanner;

public class zadanie1c {
    public static void main(String[] args){

        Scanner wczytaj = new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String str = wczytaj.nextLine();
        System.out.println(middle(str));

    }


    static String middle(String str){
        int ktore=str.length()/2;

        if (str.length()%2==0) {

            String wypisz = str.substring(ktore-1, ktore + 1);
            return wypisz;

        }else return str.substring(ktore,ktore+1);

    }
}
