package pl.edu.uwm.wmii.debskidamian.laboratorium00;


import java.util.Random;
import java.util.Scanner;


public class zad2 {
    public static void main(String[] args) {


        Scanner wczytaj = new Scanner(System.in);

        System.out.println("Ile liczb: ");
        int liczby = wczytaj.nextInt();
        System.out.println("Podaj zakres losowanych liczb (-999 - 999) ");
        int lewy = wczytaj.nextInt();
        int prawy = wczytaj.nextInt();
        int tablica[] = new int[liczby];

        generuj(tablica,liczby,lewy,prawy);
        wypisz(tablica);
        System.out.println("Liczba parzystych liczb w tablicy: " + IleParzystych(tablica) + "\nLiczba nieparzystych w tablicy: "+ IleNieparzystych(tablica) );
        System.out.println("Liczba dodatnich liczb w tablicy: " + IleDodatnich(tablica) + "\nLiczba ujemnych w tablicy: "+ IleUjemnych(tablica)+ "\nLiczba zerowych w tablicy: "+ IleZerowych(tablica) );
        System.out.println("Liczba maksymalna wystepuje w tablicy: " + IleMaksymalnych(tablica) );
        System.out.println("Suma liczb dodatnich w tablicy: " + sumaDodatnich(tablica) + "\nSuma liczb ujemnych w tablicy: "+ sumaUjemnych(tablica) );

       /*

        max = tab[0];
        for (int i = 0; i < n; i++) {

            if (tab[i] > max){
                max = tab[i];
                ilemax++;
            }
        }
    */
    }
        public static void generuj(int tab[],int n ,int minWartosc,int maxWartosc){
            Random losuj = new Random();

            for (int i=0 ; i < n; ++i){
                tab[i] = losuj.nextInt(maxWartosc - minWartosc +1) + minWartosc ;
            }
        }
        public static void wypisz(int tab[]){
            for (int i = 0; i < tab.length; i++) {

                System.out.print(tab[i] + " ");
                if (i==25 || i==50 || i==75) System.out.println("\n");
            }
            System.out.println("\n");
        }



        public static int IleNieparzystych(int tab[]) {
            int czy_nieparzyste = 0;
            for (int i = 0; i < tab.length; i++) {

                if (tab[i] % 2 != 0) {
                    czy_nieparzyste++;
                }

            }
            return czy_nieparzyste;
        }

        public static int IleParzystych(int tab[]) {
            int czy_parzyste = 0;
            for (int i = 0; i < tab.length; i++) {

                if (tab[i] % 2 == 0) {
                    czy_parzyste++;
                }

            }
            return czy_parzyste;
        }

        public static int IleDodatnich(int tab[]) {
        int dodatnie=0;
        for (int i = 0; i < tab.length; i++) {

            if (tab[i] > 0) {
                dodatnie++;
            }

        }
        return dodatnie;
    }
    public static int IleUjemnych(int tab[]) {
        int ujemne = 0;
        for (int i = 0; i < tab.length; i++) {

            if (tab[i] < 0) {
                ujemne++;
            }

        }
        return ujemne;
    }
        public static int IleZerowych(int tab[]) {
            int zerowe = 0;
            for (int i = 0; i < tab.length; i++) {

                if (tab[i] == 0) {
                    zerowe++;
                }

            }
            return zerowe;
        }
        public static int IleMaksymalnych(int tab[]) {
            int max;
            int ilemax=0;
            max = tab[0];
            for (int i = 0; i < tab.length; i++) {

                if (tab[i] > max){
                    max = tab[i];
                    ilemax++;
                }
            }
            return ilemax;
        }
        public static int sumaDodatnich(int tab[]) {
            int suma=0;
            for (int i = 0; i < tab.length; i++) {

                if (tab[i] > 0) {
                    suma=suma+tab[i];
                }

            }
            return suma;
        }
        public static int sumaUjemnych(int tab[]) {
            int suma_ujemne = 0;
            for (int i = 0; i < tab.length; i++) {

                if (tab[i] < 0) {
                    suma_ujemne=suma_ujemne+tab[i];
                }

            }
            return suma_ujemne;
        }
}