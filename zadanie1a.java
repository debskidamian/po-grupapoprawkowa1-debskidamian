package pl.edu.uwm.wmii.debskidamianlab04;

import java.util.Scanner;

public class zadanie1a {

    public static void main(String[] args) {


        Scanner wczytajstr = new Scanner(System.in);
        System.out.print("Podaj tekst: ");
        String str = wczytajstr.nextLine();
        System.out.print("\nPodaj znak ktory chcesz wyszukac w tekscie powyzej: ");
        char c = wczytajstr.next().charAt(0);
        System.out.println("Podany znak \" "+ c + " \" wystapil w tekscie " + countChar(str,c) + " razy");

    }
    static int countChar(String str, char c){
        int ilosc_wystapien=0;
        for (int i=0 ; i< str.length();i++){
            if (str.charAt(i)==c){
                ilosc_wystapien++;
            }
        }

        return ilosc_wystapien;
    }
}
