package pl.edu.uwm.wmii.debskidamian.laboratorium05;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie2 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        ArrayList<Integer> ListaTablicowa1 = new ArrayList<Integer>();
        System.out.println("Podaj dlugosc pierwszej listy tablicowej: ");
        int dlugoscListyTablicowej1 = input.nextInt();

        for (int i = 0; i < dlugoscListyTablicowej1; i++) {
            System.out.print("Podaj elementy pierwszej listy: ");
            ListaTablicowa1.add(input.nextInt());
        }

        ArrayList<Integer> ListaTablicowa2 = new ArrayList<Integer>();
        System.out.println("Podaj dlugosc drugiej listy tablicowej: ");
        int dlugoscListyTablicowej2 = input.nextInt();

        for (int i = 0; i < dlugoscListyTablicowej2; i++) {
            System.out.print("Podaj elementy drugiej listy: ");
            ListaTablicowa2.add(input.nextInt());
        }

        input.close();
        ArrayList<Integer> newArrayList = merge(ListaTablicowa1, ListaTablicowa2 );
        for (int i = 0; i < newArrayList.size(); i++) {
            System.out.print(newArrayList.get(i) + " ");
        }


    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> nowaListaTab = new ArrayList<Integer>();

        while(a.size() > 0 || b.size() > 0){
            if(a.size() > 0){
                nowaListaTab.add(a.get(0));
                a.remove(0);
            }

            if(b.size() > 0){
                nowaListaTab.add(b.get(0));
                b.remove(0);
            }

        }
        return nowaListaTab;
    }
}
