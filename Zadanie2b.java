package pl.edu.uwm.wmii.debskidamian.laboratorium01;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie2b {

    public static void main(String[] args) {

        int counter=0;
        int n=1;
        Scanner wpisz = new Scanner(System.in);

        System.out.println("Wpisz końcowy zakres przedziału : ");
        n=wpisz.nextInt();

        System.out.println("Liczby podzielne przez 3 i niepodzielne przez 5");
        for (int i=1;i<=n;i++){

            if (i%3==0 && i%5!=0) {
                counter++;

                if (counter %10==0)
                  System.out.println(i + " ");
                else
                  System.out.print(i + " ");
            }
        }

    }
}
