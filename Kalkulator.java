package pl.edu.uwm.wmii.debskidamian.laboratorium01;

import java.util.Scanner;

public class Kalkulator {

    public static void main(String[] args) {

       double liczba1, liczba2, wynik;
       System.out.println("Wpisz dwie dowolne liczby:");
	   
       Scanner wpisz = new Scanner(System.in);
       liczba1 = wpisz.nextDouble();
       liczba2 = wpisz.nextDouble();

       System.out.println("1.Dodawanie\n2.Odejmowanie\n3.Mnozenie\n4.Dzielenie\n5.Pierwiastkowanie\n6.Modulo\nWybierz jaką operację chcesz wykonać:");
       int wybor;
       wybor = wpisz.nextInt();

       switch(wybor)
       {
           case 1:wynik=dod(liczba1,liczba2);
           System.out.println("Wynik dodawania:"+wynik);
           break;

           case 2:wynik=odej(liczba1,liczba2);
               System.out.println("Wynik odejmowania:"+wynik);
               break;

           case 3:wynik=mnoz(liczba1,liczba2);
               System.out.println("Wynik mnozenia:"+wynik);
               break;

           case 4:wynik=dziel(liczba1,liczba2);
               System.out.println("Wynik dzielenia:"+wynik);
               break;

           case 5:wynik=pierw(liczba1);
               System.out.println("Wynik pierwiastkowania: "+wynik);
                break;

           case 6:wynik=mod(liczba1,liczba2);
                System.out.println("Wynik modulo: "+wynik);
                break;
       }
    }

    //dodawanie
		public static double dod(double a, double b)
		{
			return a + b;
		}
    //odejmowanie
		public static double odej(double a, double b){
			return a - b;
		}
    //mnozenie
		public static double mnoz(double a, double b){
			return a * b;
		}
    //dzielenie
		public static double dziel(double a, double b){
			if (b == 0){
				System.out.println("Nie dzielimy przez 0");
				return 0;
			}
			else{
				return a / b;
			}
		}
    //pierwiastkowanie
		public static double pierw(double a){

			if(a < 0) {
				System.out.println("Nie pierwiastkujmy ujemnych liczb");
				return 0;
			}
			else{
				return Math.sqrt(a);
			}

		}
    //modulo
		public static double mod(double a, double b){
			return a%b;
		}

}