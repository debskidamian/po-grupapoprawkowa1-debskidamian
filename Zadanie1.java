package pl.edu.uwm.wmii.debskidamian.laboratorium05;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        ArrayList<Integer> ListaTablicowa1 = new ArrayList<Integer>();
        System.out.println("Podaj dlugosc pierwszej listy tablicowej: ");
        int dlugoscListyTablicowej1 = input.nextInt();

        for(int i = 0; i < dlugoscListyTablicowej1; i++){
            System.out.print("Podaj elementy pierwszej listy: ");
            ListaTablicowa1.add(input.nextInt());
        }

        ArrayList<Integer> ListaTablicowa2 = new ArrayList<Integer>();
        System.out.println("Podaj dlugosc drugiej listy tablicowej: ");
        int dlugoscListyTablicowej2 = input.nextInt();

        for(int i = 0; i < dlugoscListyTablicowej2; i++){
            System.out.print("Podaj elementy drugiej listy: ");
            ListaTablicowa2.add(input.nextInt());
        }

        input.close();
        ArrayList<Integer> nowaListaTab = append(ListaTablicowa1,ListaTablicowa2);
        System.out.print(nowaListaTab);

    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> nowaListaTab = new ArrayList<Integer>();

        for(int i = 0; i < a.size(); i++){
            nowaListaTab.add(a.get(i));
        }

        for(int i = 0; i < b.size(); i++){
            nowaListaTab.add(b.get(i));
        }
        return nowaListaTab;
    }

    public static void wyswietl(ArrayList<Integer> nowaListaTab){
        for(int i = 0; i < nowaListaTab.size(); i++) {
            System.out.print(nowaListaTab.get(i) + " ");
        }
    }
}
