package pl.edu.uwm.wmii.debskidamianlab04;

import java.util.Scanner;

public class zadanie1b {
    public static void main(String[] args) {
        Scanner wczytaj = new Scanner(System.in);
        System.out.println("Podaj pierwszy tekst: ");
        String str = wczytaj.nextLine();
        System.out.println("Podaj drugi tekst: ");
        String subStr = wczytaj.nextLine();

        if (str.length() > subStr.length()) {
            System.out.println("Napis " + subStr + " wystepuje w " + str + "; " + countSubStr(str, subStr)+" razy");
        } else System.out.println("tekst drugi jest zbyt długi aby mogł znaleźć się w pierwszym");

    }

    static int countSubStr(String str, String subStr) {
        int czyzawiera = 0,ilezawiera=0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i)==subStr.charAt(0)){
                int j=i;
                for (int k=0; k< subStr.length(); k++,j++){
                    if (str.charAt(j)==subStr.charAt(k)){
                        czyzawiera++;

                    }
                    if (czyzawiera==subStr.length()) ilezawiera++;

                }
                czyzawiera=0;
            }
        }
        return ilezawiera;
    }
}